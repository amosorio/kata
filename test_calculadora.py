from unittest import TestCase
from Calculadora import Calculadora
__author__ = 'ADRIANA'


class TestCalculadora(TestCase):
  pass
  def test_suma1(self):
        instancia = Calculadora()
        result = instancia.suma("")
        self.assertEqual(result,0,"Cadena vacia")

  def test_suma2(self):
        instancia = Calculadora()
        result = instancia.suma("1")
        self.assertEqual(result,1,"Un Numero")

  def test_suma3(self):
        instancia = Calculadora()
        result = instancia.suma("1,2")
        self.assertEqual(result,3,"dos")

  def test_suma4(self):
        instancia = Calculadora()
        result = instancia.suma("1,2,3")
        self.assertEqual(result,6,"Tres numeros")

  def test_suma4(self):
        instancia = Calculadora()
        result = instancia.suma("1,2,3,4")
        self.assertEqual(result,10,"Tres numeros")

  def test_suma5(self):
        instancia = Calculadora()
        result = instancia.suma("1&2&3")
        self.assertEqual(result,6,"Tres numeros con separados &")

  def test_suma5(self):
        instancia = Calculadora()
        result = instancia.suma("1:2:3")
        self.assertEqual(result,6,"Tres numeros con separador :")